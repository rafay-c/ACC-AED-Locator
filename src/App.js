import React, { useState } from 'react';
import './App.css';
import { GoogleMap, withScriptjs, withGoogleMap, Marker, InfoWindow } from "react-google-maps";
import * as aedData from "./data/aed-location.json";

function Map() {
  const [selectedAed, setSelectedAed] = useState(null);

  return (
    <GoogleMap
      defaultZoom={10}
      defaultCenter={{ lat: 21.330316, lng: -157.922402 }}
    >
      {aedData.features.map(aed => (
        <Marker key={aed.properties.AED_ID}
          position={{
            lat: aed.geometry.coordinates[1],
            lng: aed.geometry.coordinates[0]
          }}
          onClick={() => {
            setSelectedAed(aed);
          }}
        />
      ))}

      {selectedAed && (
        <InfoWindow
          position={{
            lat: selectedAed.geometry.coordinates[1],
            lng: selectedAed.geometry.coordinates[0]
          }}
          onCloseClick={() => {
            setSelectedAed(null);
          }}
        >
          <div>
            <h1>{selectedAed.properties.NAME}</h1>
            <h2>{selectedAed.properties.NNAME}</h2>
            <p>{selectedAed.properties.NOTES}</p>
            <p>
              <h3>Goole Maps:</h3>
              <ul><a href={`https://maps.google.com/?q=${selectedAed.geometry.coordinates[1]},${selectedAed.geometry.coordinates[0]}`} target="_blank" rel="noopener noreferrer">
                {selectedAed.properties.ADDRESS}
              </a></ul>
            </p>
            <p>
              <h3>Apple Maps:</h3>
              <ul><a href={`https://maps.apple.com/?q=${selectedAed.geometry.coordinates[1]},${selectedAed.geometry.coordinates[0]}`} target="_blank" rel="noopener noreferrer">
                {selectedAed.properties.ADDRESS}
              </a></ul>
            </p>
          </div>
        </InfoWindow>
      )}
    </GoogleMap>
  );
}

const WrappedMap = withScriptjs(withGoogleMap(Map));

function App() {
  return (
    <div style={{ width: '100vw', height: '95vh' }}>
      <div style={{ textAlign: 'center' }}>
        <h1>AED Locator</h1>
      </div>
      <WrappedMap
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyDjTdQEIWWJNHlxrXZ5O_l3PiPKwNpd3_A`}
        loadingElement={<div style={{ height: "100%" }} />}
        containerElement={<div style={{ height: "100%" }} />}
        mapElement={<div style={{ height: "100%" }} />}
      />
    </div>
  );
}

export default App;
